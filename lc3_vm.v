module lc3_vm(
	input clk_in,
	input rst_in,
	input usr_btn,
	output reg [7:0] led_out
);
	reg [31:0] counter = 32'b0;
	always @(posedge clk_in)
		if (!rst_in)
			led_out <= 8'hAA;
		else begin
			if (!usr_btn) begin
				led_out <= 8'b0;
				counter <= 32'b0;
			end
			else begin				
				if (counter < 12000000)
					counter <= counter + 1;
				else begin
					if (led_out < 255)
						led_out <= led_out + 8'b1;
					else
						led_out <= 0;
					counter <= 0;
				end
			end
		end
endmodule

